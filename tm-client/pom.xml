<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.t1.nkiryukhin.tm</groupId>
    <artifactId>task-manager-client</artifactId>
    <version>1.32.0</version>

    <parent>
        <groupId>ru.t1.nkiryukhin.tm</groupId>
        <artifactId>task-manager</artifactId>
        <version>1.32.0</version>
    </parent>

    <name>Task Manager Client</name>

    <developers>
        <developer>
            <id>nkiryukhin</id>
            <name>Nikita Kiryukhin</name>
            <email>nkiryukhin@t1-consulting.ru</email>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <dependencies>
		<dependency>
			<groupId>ru.t1.nkiryukhin.tm</groupId>
			<artifactId>task-manager-domain</artifactId>
			<version>1.32.0</version>
		</dependency>
        <dependency>
            <groupId>com.jcabi</groupId>
            <artifactId>jcabi-manifests</artifactId>
            <version>1.1</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.5</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.7.5</version>
        </dependency>
        <dependency>
            <groupId>org.reflections</groupId>
            <artifactId>reflections</artifactId>
            <version>0.9.11</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>22.0.0</version>
        </dependency>
    </dependencies>

    <build>
        <finalName>task-manager</finalName>
        <plugins>
            <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <version>4.9.10</version>
                <executions>
                    <execution>
                        <id>get-the-git-infos</id>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>validate-the-git-infos</id>
                        <goals>
                            <goal>validateRevision</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <generateGitPropertiesFile>true</generateGitPropertiesFile>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <classpathPrefix>../lib/</classpathPrefix>
                            <mainClass>ru.t1.nkiryukhin.tm.Application</mainClass>
                        </manifest>
                        <manifestEntries>
                            <groupId>${groupId}</groupId>
                            <artifactId>${artifactId}</artifactId>
                            <version>${version}</version>

                            <buildNumber>${buildNumber}</buildNumber>
                            <timestamp>${timestamp}</timestamp>

                            <developer>Nikita Kiryukhin</developer>
                            <email>nkiryukhin@t1-consulting.ru</email>

                            <!--suppress UnresolvedMavenProperty -->
                            <buildHash>${git.commit.id.abbrev}</buildHash>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitBranch>${git.branch}</gitBranch>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitId>${git.commit.id}</gitCommitId>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitTime>${git.commit.time}</gitCommitTime>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitMessage>${git.commit.message.full}</gitCommitMessage>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitterName>${git.commit.user.name}</gitCommitterName>
                            <!--suppress UnresolvedMavenProperty -->
                            <gitCommitterEmail>${git.commit.user.email}</gitCommitterEmail>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>1.4</version>
                <executions>
                    <execution>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <doCheck>true</doCheck>
                    <doUpdate>false</doUpdate>
                    <revisionOnScmFailure>true</revisionOnScmFailure>
                    <format>${version}.{0,number}</format>
                    <items>
                        <item>buildNumber</item>
                        <item>timestamp</item>
                    </items>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-dependencies</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/app/lib</outputDirectory>
                            <overWriteReleases>false</overWriteReleases>
                            <overWriteSnapshots>false</overWriteSnapshots>
                            <overWriteIfNewer>true</overWriteIfNewer>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.0.1</version>
                <executions>
                    <execution>
                        <id>copy</id>
                        <phase>install</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <artifactItems>
                        <artifactItem>
                            <groupId>ru.t1.nkiryukhin.tm</groupId>
                            <artifactId>task-manager</artifactId>
                            <version>1.30.0</version>
                            <type>jar</type>
                            <overWrite>true</overWrite>
                            <outputDirectory>${project.build.directory}/app/bin</outputDirectory>
                            <destFileName>app.jar</destFileName>
                        </artifactItem>
                    </artifactItems>
                    <overWriteReleases>false</overWriteReleases>
                    <overWriteSnapshots>true</overWriteSnapshots>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <scm>
        <connection>scm:svn:http://127.0.0.1/dummy</connection>
        <developerConnection>scm:svn:https://127.0.0.1/dummy</developerConnection>
        <tag>HEAD</tag>
        <url>http://127.0.0.1/dummy</url>
    </scm>

</project>
