package ru.t1.nkiryukhin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.TaskStartByIndexRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest();
        request.setIndex(index);
        getTaskEndpoint().startTaskByIndex(request);
    }

}
