package ru.t1.nkiryukhin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.endpoint.IProjectEndpointClient;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectEndpointClient getProjectEndpoint() {
        return serviceLocator.getProjectEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
