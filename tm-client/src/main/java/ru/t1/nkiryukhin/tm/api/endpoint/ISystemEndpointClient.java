package ru.t1.nkiryukhin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {

    void setSocket(@Nullable Socket socket);

}
