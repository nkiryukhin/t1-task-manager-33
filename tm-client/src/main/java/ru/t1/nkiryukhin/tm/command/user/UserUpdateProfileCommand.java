package ru.t1.nkiryukhin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.UserUpdateProfileRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    private final String DESCRIPTION = "update profile of current user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME");
        @NotNull final String middleName = TerminalUtil.nextLine();

        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest();
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        getUserEndpoint().updateUserProfile(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
