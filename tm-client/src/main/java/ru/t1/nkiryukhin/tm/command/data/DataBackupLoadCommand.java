package ru.t1.nkiryukhin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.request.DataBackupLoadRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    public static final String NAME = "backup-load";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load backup from file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD]");
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest();
        getDomainEndpointClient().loadDataBackup(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
