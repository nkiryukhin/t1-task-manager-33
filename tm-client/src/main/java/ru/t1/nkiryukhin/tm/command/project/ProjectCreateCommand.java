package ru.t1.nkiryukhin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.ProjectCreateRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest();
        request.setName(name);
        request.setDescription(description);

        getProjectEndpoint().createProject(request);
    }

}
