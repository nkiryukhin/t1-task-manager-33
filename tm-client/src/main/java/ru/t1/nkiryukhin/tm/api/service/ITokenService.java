package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    @Nullable
    String getToken();

    void setToken(@Nullable String token);

}
