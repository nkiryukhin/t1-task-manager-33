package ru.t1.nkiryukhin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.ApplicationVersionRequest;
import ru.t1.nkiryukhin.tm.dto.response.ApplicationVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show application version";
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse response = getSystemEndpoint().getVersion(request);
        System.out.println(response.getVersion());
    }

}
