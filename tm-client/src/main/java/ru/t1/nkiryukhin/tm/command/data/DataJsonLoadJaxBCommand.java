package ru.t1.nkiryukhin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.request.DataJsonLoadJaxBRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from json file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-json-jaxb";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest();
        getDomainEndpointClient().loadDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
