package ru.t1.nkiryukhin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.request.ProjectListRequest;
import ru.t1.nkiryukhin.tm.dto.response.ProjectListResponse;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Display all projects";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(sort);

        @NotNull ProjectListResponse response = getProjectEndpoint().listProject(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @NotNull final List<Project> projects = response.getProjects();
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
