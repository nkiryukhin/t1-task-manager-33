package ru.t1.nkiryukhin.tm.dto.response;

import ru.t1.nkiryukhin.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(User user) {
        super(user);
    }

}