package ru.t1.nkiryukhin.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Project;

public final class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@Nullable Project project) {
        super(project);
    }

}