package ru.t1.nkiryukhin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskUpdateByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    @NotNull
    private String name;

    @NotNull
    private String description;

}
