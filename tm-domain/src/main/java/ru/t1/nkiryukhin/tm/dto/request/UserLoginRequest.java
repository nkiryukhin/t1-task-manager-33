package ru.t1.nkiryukhin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@AllArgsConstructor
public final class UserLoginRequest extends AbstractUserRequest {

    @NotNull
    private String login;

    @NotNull
    private String password;

}