package ru.t1.nkiryukhin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class UserLogoutRequest extends AbstractUserRequest {
    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }
}