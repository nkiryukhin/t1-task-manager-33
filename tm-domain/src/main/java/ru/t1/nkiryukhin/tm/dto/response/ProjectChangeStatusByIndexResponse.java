package ru.t1.nkiryukhin.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Project;

public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable Project project) {
        super(project);
    }

}