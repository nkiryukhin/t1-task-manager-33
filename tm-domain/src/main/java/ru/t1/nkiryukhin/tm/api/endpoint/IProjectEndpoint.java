package ru.t1.nkiryukhin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.dto.response.*;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider){
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }


    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectGetByIdResponse getProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectGetByIndexResponse getProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) throws AbstractException;

}
