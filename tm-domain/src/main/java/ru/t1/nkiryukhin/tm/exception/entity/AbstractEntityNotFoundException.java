package ru.t1.nkiryukhin.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException(@NotNull String message) {
        super(message);
    }

}
