package ru.t1.nkiryukhin.tm.api.service;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface IDomainService {

    void loadDataBackup() throws IOException;

    void saveDataBackup() throws IOException;

    void loadDataBase64() throws IOException, ClassNotFoundException;

    void saveDataBase64() throws IOException;

    void loadDataBinary() throws IOException, ClassNotFoundException;

    void saveDataBinary() throws IOException;

    void loadDataJsonFasterXml() throws IOException;

    void loadDataJsonJaxB() throws JAXBException;

    void saveDataJsonFasterXml() throws IOException;

    void saveDataJsonJaxB() throws IOException, JAXBException;

    void loadDataXmlFasterXml() throws IOException;

    void loadDataXmlJaxB() throws JAXBException;

    void saveDataXmlFasterXml() throws IOException;

    void saveDataXmlJaxB() throws IOException, JAXBException;

    void loadDataYamlFasterXml() throws IOException;

    void saveDataYamlFasterXml() throws IOException;

}

