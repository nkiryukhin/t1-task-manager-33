package ru.t1.nkiryukhin.tm.api.repository;

import ru.t1.nkiryukhin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
