package ru.t1.nkiryukhin.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.service.IServiceLocator;
import ru.t1.nkiryukhin.tm.dto.request.AbstractUserRequest;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.model.Session;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

//    protected void check(
//            @Nullable final AbstractUserRequest request,
//            @Nullable final Role role
//    ) throws AccessDeniedException, AbstractFieldException {
//        if (request == null) throw new AccessDeniedException();
//        if (role == null) throw new AccessDeniedException();
//        @Nullable final String userId = request.getUserId();
//        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
//        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
//        @NotNull final IUserService userService = serviceLocator.getUserService();
//        @Nullable final User user = userService.findOneById(userId);
//        if (user == null) throw new AccessDeniedException();
//        @Nullable final Role roleUser = user.getRole();
//        final boolean check = roleUser == role;
//        if (!check) throw new AccessDeniedException();
//    }

    @NotNull
    protected Session check(@Nullable final AbstractUserRequest request) throws AccessDeniedException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

}
