package ru.t1.nkiryukhin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.endpoint.*;
import ru.t1.nkiryukhin.tm.api.repository.IProjectRepository;
import ru.t1.nkiryukhin.tm.api.repository.ISessionRepository;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.api.repository.IUserRepository;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.endpoint.*;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;
import ru.t1.nkiryukhin.tm.repository.SessionRepository;
import ru.t1.nkiryukhin.tm.repository.TaskRepository;
import ru.t1.nkiryukhin.tm.repository.UserRepository;
import ru.t1.nkiryukhin.tm.service.*;
import ru.t1.nkiryukhin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";                 //вынести в проперти
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() throws AbstractException {
        @NotNull final User user = userService.create("user", "123", "user1@mail.com");
        @NotNull final User nkiryukhin = userService.create("nkiryukhin", "666", "na@mail.com");
        @NotNull final User admin = userService.create("admin", "sadmin", Role.ADMIN);

        projectService.add(user.getId(), new Project("proj2", "project two", Status.IN_PROGRESS));
        projectService.add(nkiryukhin.getId(), new Project("proj1", "project one", Status.COMPLETED));
        projectService.add(nkiryukhin.getId(), new Project("proj4", "project four", Status.NOT_STARTED));
        projectService.add(admin.getId(), new Project("proj3", "project three", Status.IN_PROGRESS));

        taskService.create(user.getId(), "t2", "task two", Status.IN_PROGRESS);
        taskService.create(nkiryukhin.getId(), "t1", "task one", Status.COMPLETED);
        taskService.create(nkiryukhin.getId(), "t4", "task four", Status.NOT_STARTED);
        taskService.create(admin.getId(), "t3", "task three", Status.IN_PROGRESS);
    }

    public void run() {
        initPID();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        try {
            initDemoData();
            backup.start();
        } catch (Exception e) {
            loggerService.error(e);
        }
    }

    public void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}