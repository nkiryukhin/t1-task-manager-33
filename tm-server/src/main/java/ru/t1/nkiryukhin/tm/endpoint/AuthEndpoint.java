package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IAuthService;
import ru.t1.nkiryukhin.tm.api.service.IServiceLocator;
import ru.t1.nkiryukhin.tm.dto.request.UserLoginRequest;
import ru.t1.nkiryukhin.tm.dto.request.UserLogoutRequest;
import ru.t1.nkiryukhin.tm.dto.request.UserProfileRequest;
import ru.t1.nkiryukhin.tm.dto.response.UserLoginResponse;
import ru.t1.nkiryukhin.tm.dto.response.UserLogoutResponse;
import ru.t1.nkiryukhin.tm.dto.response.UserProfileResponse;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) throws AbstractException {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) throws AbstractException {
        final Session session = check(request);
        getServiceLocator().getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @Override
    public @NotNull UserProfileResponse profile(@NotNull UserProfileRequest request) {
        return null;
    }

}